<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"/> -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdleft">
					<img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Main Logo">
				</div>
				<div class="hdRight">
					<div class="top">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
							</ul>
						</nav>
					</div>
					<div class="bot">
						<div class="social">
							<a href="<?php $this->info("fb_link"); ?>" target="_blank"><img class="facebook" src="public/images/common/sprite.png" alt="facebook" /></a>
							<a href="<?php $this->info("ig_link"); ?>" target="_blank"><img class="instagram" src="public/images/common/sprite.png" alt="instagram" /></a>
							<a href="<?php $this->info("gp_link"); ?>" target="_blank"><img class="googlep" src="public/images/common/sprite.png" alt="google plus" /></a>
							<a href="<?php $this->info("yp_link"); ?>" target="_blank"><img class="yelp" src="public/images/common/sprite.png" alt="yelp" /></a>
						</div>
						<div class="hdPhone">
							<?php $this->info(["phone","tel"]); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="banner-text">
				<h2>Dealer Quality Service – Without The Dealer Price!</h2>
				<p>Bring your BMW to our experienced BMW technician today. Get superior service for a competitive price!</p>
				<img src="public/images/common/bmw-banner.png" alt="BMW" class="bmw">
			</div>
		</div>
	<?php //endif; ?>
