<div id="content">
	<div id="section1">
		<div class="row">
			<div class="left">
				<div class="left-top">
					<div class="box1">
						<h1>FEATURED SERVICE</h1>
						<p>Don’t trust your BMW with anyone else! We’ve got the experience needed to maintain your vehicle.</p>
						<a href="service#content" class="btn">LEARN MORE</a>
					</div>
					<div class="box2 svc">
						<img class="service1" src="public/images/content/service1.jpg" alt="service1" />
						<div class="text">
							<h3>OIL CHANGES  $89</h3>
							<p>We use BMW full synthetic <br>oil and BMW oil filter</p>
						</div>
					</div>
				</div>
				<div class="left-mid">
					<div class="box3">
						<div class="svc">
							<img class="service2" src="public/images/content/service2.jpg" alt="service2" />
							<div class="text">
								<h3>BRAKE WORK</h3>
							</div>
						</div>
						<div class="svc">
							<img class="service3" src="public/images/content/service3.jpg" alt="service3" />
							<div class="text">
								<h3>AC SERVICE</h3>
							</div>
						</div>
					</div>
					<div class="box4 svc">
						<img class="service4" src="public/images/content/service4.jpg" alt="service4" />
						<div class="text">
							<h3>TUNE-UPS</h3>
						</div>
					</div>
				</div>
				<div class="left-bot">
					<div class="box5 svc">
						<img class="service4" src="public/images/content/service5.jpg" alt="service5" />
						<div class="text">
							<h3>SHOCKS & STRUTS</h3>
						</div>
					</div>
					<div class="box6">
						<h2>All work guaranteed</h2>
						<p>With over 25 years’ experience working with bimmers, you can trust our BMW certified technicians to always provide you with quality work. Whether you are in need of simple routine maintenance work or a more extensive repair, we can take care of it for you!</p>
						<a href="services#content" class="btn">LEARN MORE</a>
					</div>
				</div>
			</div>
			<div class="right">
				<div class="right-top">
					<img class="engine" src="public/images/content/engine.png" alt="engine" />
				</div>
				<div class="right-mid">
					<h2>Complete service for your BMW</h2>
					<p>Our talented and experienced BMW technician provides the full service you need to maintain your vehicle.</p>
					<p>Bring your BMW in to our shop for any repair that you may need, and also for complete factory service and warning light resets. We look forward to meeting you!</p>
					<div class="button">
						<a href="service#content" class="btn">LEARN MORE</a>
						<a href="#" target="_blank"><img class="bimmer" src="public/images/common/bimmer.png" alt="bimmer" /></a>
					</div>
				</div>
				<div class="right-bot svc">
					<img class="service6" src="public/images/content/service6.jpg" alt="service6" />
					<div class="text">
						<h3>ALIGNMENTS</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="section2">
		<div class="row">
			<div class="container">
				<div class="left">
					<img src="public/images/content/car-engine.png" alt="Car Engine">
				</div>
				<div class="right">
					<div class="text">
						<h2>Over 15 years of BMW experience!</h2>
						<h4>Serving the entire Dallas / Fort Worth, TX area.</h4>
						<h2>What is DFW Bimmer all about?</h2>
						<p>At DFW Bimmer, we perform regular maintenance on your BMW to ensure that all parts are working properly. We check all of your fluid levels, replace worn-out parts, and notify you of potential problems.</p>
						<p>Don’t wait to have your BMW serviced by our friendly professionals. Schedule an appointment today. We can perform any service your BMW needs!</p>
						<a href="services#content" class="btn">LEARN MORE</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="section3">
		<div class="row">
			<div class="left">
				<h3>Top-notch repairs and maintenance works for any BMW</h3>
				<p>Bring your car in to DFW Bimmer for the excellent, honest, and competitively priced work that you are looking for to maintain your BMW. After all, your car is more than just a mode of transportation. It is a beautiful machine that can be yours to treasure for years.</p>
				<p>Just give us a call or browse through our testimonials, and see for yourself why the Dallas / Fort Worth, TX area relies on us for all of their BMW needs.</p>
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
			<div class="right">
				<img src="public/images/content/section3.jpg" alt="carS3" class="section3IMG">
			</div>
		</div>
	</div>
	<div id="section4">
		<div class="row">
			<div class="left">
				<div class="left-top">
					<div class="box1">
						<h5>OUR GALLERY</h5>
						<h2>RECENT WORKS</h2>
						<a href="gallery#content" class="btn">VIEW MORE</a>
					</div>
					<div class="box2">
						<img class="gallery1" src="public/images/content/gallery1.jpg" alt="gallery image 1" />
					</div>
				</div>
				<div class="left-bot">
					<div class="box3">
						<img class="gallery2" src="public/images/content/gallery2.jpg" alt="gallery image 2" />
						<img class="gallery3" src="public/images/content/gallery3.jpg" alt="gallery image 3" />
					</div>
					<div class="box4">
						<img class="gallery4" src="public/images/content/gallery4.jpg" alt="gallery image 4" />
					</div>
				</div>
			</div>
			<div class="right">
				<h2>CLIENT REVIEWS</h2>
				<div class="review">
					<p class="author"><span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span> – Brad Holmes</p>
					<p class="comment">Jesse is a great, honest tech. Has always delivered the highest standard of service, while delivering exceptional value with such competitive pricing. He’s always been super flexible, knowledgeable, and even educational taking the time to tell you all the things the salesman doesn’t. A+++, 5 Star Service. Will not use another tech as long as I own a BMW. </p>
				</div>
				<div class="review">
					<p class="author"><span class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</span> – David Kent</p>
					<p class="comment">Jesse first repaired my BMW about six months ago. He quickly became my single source for service, repair and maintenance. His work is reliable and his rates are reasonable. You can’t ask for anything more from a highly qualified BMW Technician like Jesse. Try him. He’ll not disappoint. </p>
				</div>
				<a href="testimonials#content" class="btn">READ MORE</a>
			</div>
		</div>
	</div>
	<div id="section5">
		<div class="row">
			<div class="container">
				<h3>FREE ESTIMATE</h3>
				<h2>REQUEST A QUOTE</h2>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="NAME:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="EMAIL:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="PHONE:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="MESSAGE:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn2" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
	</div>
</div>
